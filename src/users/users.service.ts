import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto) {
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({ where: { id } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const User = await this.usersRepository.findOneBy({ id });
    if (!User) {
      throw new NotFoundException();
    }
    const updatedUser = { ...User, ...updateUserDto };

    return this.usersRepository.save(updatedUser);
  }

  async remove(id: number) {
    const User = await this.usersRepository.findOneBy({ id });
    if (!User) {
      throw new NotFoundException();
    }
    return this.usersRepository.softRemove(User);
  }
}
